<?php
/**
 * Slim 3 Maintenance Middleware
 *
 * MIT LICENSE
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
namespace Twardiii\Middleware;

/**
 * Maintenance Middleware
 *
 * This middleware renders a maintenance when the Slim 3 application
 * mode is set to 'maintenance'.
 */
class Maintenance
{
    /**
     * @var callable
     */
     protected $callable;

    /**
     * Constructor
     * @param callable $callable
     */
    public function __construct($callable = null)
    {
        if (null === $callable) {
            $this->callable = array($this, 'defaultMaintenancePage');
        } else {
            if (!is_callable($callable)) {
                throw new \InvalidArgumentException('argument callable must be callable');
            } else {
                $this->callable = $callable;
            }
        }
    }

    /**
     * Example middleware invokable class
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke($request, $response, $next)
    {
        $mode = getenv('SLIM_MODE');
        if ($mode === 'maintenance') {
            call_user_func_array($this->callable, array($request, &$response));
        } else {
            $response = $next($request, $response);
        }

        return $response;
    }

    /**
     * Default maintenance callback
     */
    public function defaultMaintenancePage($request, &$response)
    {
        $body_text = "<html><head><title>Maintenance</title><style>body{margin:0;padding:30px;font:12px/1.5 Helvetica,Arial,Verdana,sans-serif;}h1{margin:0;font-size:48px;font-weight:normal;line-height:48px;}strong{display:inline-block;width:65px;}</style></head><body><h1>Maintenance</h1><p>Please try again later.</p></body></html>";
        $response->withStatus(503);
        $body = $response->getBody();
        $body->write($body_text);
    }
}
